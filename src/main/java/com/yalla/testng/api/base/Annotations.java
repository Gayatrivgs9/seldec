package com.yalla.testng.api.base;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import com.yalla.selenium.api.base.SeleniumBase;

import utils.DataLibrary;

public class Annotations extends SeleniumBase {
	
	@DataProvider(name = "fetchData")
	public Object[][] fetchData() throws IOException {
		return DataLibrary.readExcelData(excelFileName);
	}
	
  @Parameters({"url", "username"})
  @BeforeMethod(groups = {"any"})
  public void beforeMethod(String url, String username) {
	startApp("chrome", url);
	WebElement eleUserName = locateElement("id", "username");
	clearAndType(eleUserName, username);
	WebElement elePassword = locateElement("id", "password");
	clearAndType(elePassword, "crmsfa");
	WebElement eleLogin = locateElement("class", "decorativeSubmit");
	click(eleLogin);
	click(locateElement("link", "CRM/SFA"));
  }

  @AfterMethod(groups = {"any"})
  public void afterMethod() {
	  close();
  }

}
