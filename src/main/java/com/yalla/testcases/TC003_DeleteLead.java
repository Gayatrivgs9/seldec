package com.yalla.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.yalla.selenium.api.base.SeleniumBase;

public class TC003_DeleteLead extends SeleniumBase{
	@Test
	public void deleteLead() throws InterruptedException {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		clearAndType(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		click(locateElement("link", "CRM/SFA"));
		click(locateElement("link", "Leads"));
		click(locateElement("link", "Find Leads"));
		click(locateElement("xpath","//span[text()='Phone']"));
		clearAndType(locateElement("name", "phoneNumber"), "99"); 
	    click(locateElement("xpath","//button[text()='Find Leads']"));
	    Thread.sleep(1000);
	    String text = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").getText();
	    click(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
	    click(locateElement("link", "Delete"));
	    click(locateElement("link", "Find Leads"));
	    clearAndType(locateElement("xpath", "//input[@name='id']"), text);
	    click(locateElement("xpath","//button[text()='Find Leads']"));
	    verifyExactText(locateElement("class","x-paging-info"), "No records to display");
	}
	
}














